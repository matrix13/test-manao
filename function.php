<?php
    function getUser($login) {
        $xml = simplexml_load_file("data.xml");
        foreach ($xml->user as $users){
            if ($users->login == $login) return $users;
        }
        return null;
    }

    function getUserFromEmail($email) {
        $xml = simplexml_load_file("data.xml");
        foreach ($xml->user as $users){
            if ($users->email == $email) return $users;
        }
        return null;
    }

    function onRegister($login, $password, $email, $name){
        $dom_xml= new DOMDocument('1.0', 'utf-8');
        $dom_xml->formatOutput = true;
        $dom_xml->load("data.xml");

        $user = $dom_xml->createElement('user');
        $user->appendChild($dom_xml->createElement("login", $login));
        $user->appendChild($dom_xml->createElement("password", $password));
        $user->appendChild($dom_xml->createElement("name", $name));
        $user->appendChild($dom_xml->createElement("email", $email));

        $dom_xml->getElementsByTagName("users") -> item(0) -> appendChild($user);
        $dom_xml->save("data.xml");
    }

