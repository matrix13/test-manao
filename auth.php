<!DOCTYPE html>
<html>
<head>
    <title>Тестовое задание</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <script type="text/javascript" src="/js/jquery.js"></script>
    <script type="text/javascript" src="/js/form.js"></script>

    <link rel="stylesheet" type="text/css" href="/styles/main.css" />
</head>
<body>
    <div style="text-align: center">
        <div id="info"></div>
        <h1>Авторизация</h1>
        <form id="auth" action="" method="post">
            <p>Логин</p>
            <p><input type="text" name="login"></p>
            <p>Пароль</p>
            <p><input type="password" name="password"></p>
            <br>
            <p>
                <a class="btn" onclick="sendForm('auth', '/index.php?go=form&name=auth')">Войти</a>
                <a style="margin-left: 10px;" href="/index.php?go=register" class="btn">Регистрация</a>
            </p>
        </form>
    </div>
</body>
</html>