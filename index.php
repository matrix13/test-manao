<?php
    session_start();

    @ini_set('display_errors', false);
    @ini_set('html_errors', false);

    define('DOMAIN', null);

    include_once 'function.php';
    include_once 'checkauth.php';

    $go = $_GET['go'];
    if($go == "register") {
        include('register.php');
        exit();
    } else if($go == "form") {
        include('form.php');
        exit();
    } else if($go == "logout") {
        setcookie("login", "", time(), "/");
        setcookie("password", "", time(), "/");

        $_SESSION['login'] = null;
        $_SESSION['password'] = null;

        $user = null;

        header ('Location: /index.php');
    }

    if ($is_logged) {
        include('cabinet.php');
    } else {
        include('auth.php');
    }
