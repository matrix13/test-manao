function sendForm(form, url) {
	var data = $("#" + form).serialize();
	$.ajax({
		type: "POST",
		url: url,
		data: data,
		success: function(data) {
			console.log(data);

			var obj = JSON.parse(data);

			if(obj.message != null) setInfoMessage('info', obj.message);
			if(obj.redirect != null) document.location.replace(obj.redirect);
		},
		error: function(xhr, str){
			setInfoMessage('info', 'Ошибка запроса.');
		}
	});
}

function setInfoMessage(element, message){
	$("#" + element).html('<span>' + message + '</span>');
}