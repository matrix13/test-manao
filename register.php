<!DOCTYPE html>
<html>
<head>
	<title>Регисрация</title>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8"> 
	<script type="text/javascript" src="/js/jquery.js"></script>
	<script type="text/javascript" src="/js/form.js"></script>

    <link rel="stylesheet" type="text/css" href="/styles/main.css" />
</head>
<body>
	<div style="text-align: center">
        <div id="info"></div>
		<h1>Регистрация</h1>
		<form id="register" action="" method="post">
			<p>Логин</p>
			<p><input type="text" name="login"></p>
			<p>Пароль</p>
			<p><input type="password" name="password"></p>
			<p>Повторите пароль</p>
			<p><input type="password" name="password2"></p>
			<p>Email</p>
			<p><input type="Email" name="email"></p>
			<p>Имя</p>
			<p><input type="text" name="name"></p>
            <br>
			<p>
                <a class="btn" onclick="sendForm('register', '/index.php?go=form&name=register')">Регистрация</a>
                <a class="btn" style="margin-left: 10px;" href="/index.php">Назад</a>
            </p>
		</form>
	</div>
	
</body>
</html>