<?php

    $name = $_GET['name'];
    if($name == "register") {
        if($_POST['login'] AND $_POST['password'] AND $_POST['password2'] AND $_POST['email'] AND $_POST['name']) {
            $userCheck = getUser($_POST['login']);
            if ($userCheck == null) {
                if($_POST['password'] == $_POST['password2']) {
                    $userCheck = getUserFromEmail($_POST['email']);
                    if ($userCheck == null) {
                        $password_hash = md5("more_guard_123" . $_POST['password']);
                        onRegister($_POST['login'], $password_hash, $_POST['email'], $_POST['name']);
                        exit(json_encode(array("status" => "success", "message" => "Регистрация успешно завершина. <a href='/'>Перейти к авторизации</a>")));
                    } else exit(json_encode(array("status" => "error", "message" => "Пользователь с таким Email уже зарегистрирован.")));
                } else exit(json_encode(array("status" => "error", "message" => "Повторенный пароль не совпадает с паролем")));
            } else exit(json_encode(array("status" => "error", "message" => "Пользователь с таким логином уже зарегистрирован.")));
        } else exit(json_encode(array("status" => "error", "message" => "Форма заполнена некорректно!")));
    } else if($name == "auth") {
        if($_POST['login'] AND $_POST['password']) {
            $user = getUser($_POST['login']);
            if($user != null) {
                $password_hash = md5("more_guard_123".$_POST['password']);
                if($user->password == $password_hash) {

                    setcookie("login", $user->login, time() + 86400 * 365, "/");
                    setcookie("password", $password_hash, time() + 86400 * 365, "/");

                    $_SESSION['login'] = $user->login;
                    $_SESSION['password'] = $password_hash;

                    exit(json_encode(array("status" => "success", "redirect" => "/", "message" => "Авторизация прошла успешно. Переходи в кабинет!")));
                } else exit(json_encode(array("status" => "error", "message" => "Пароль введен неверно! ")));
            } else exit(json_encode(array("status" => "error", "message" => "Пользователь не найден!")));
        } else exit(json_encode(array("status" => "error", "message" => "Логин и пароль не могут быть пустыми!")));
    }

    exit();