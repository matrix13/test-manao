<!DOCTYPE html>
<html>
<head>
    <title>Тестовое задание</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <script type="text/javascript" src="/js/jquery.js"></script>
    <script type="text/javascript" src="/js/form.js"></script>

    <link rel="stylesheet" type="text/css" href="/styles/main.css" />
</head>
<body>
<div style="text-align: center">
    <span>Hello <?php echo $user->name; ?></span>
    <br>
    <br>
    <br>
    <a href="/index.php?go=logout" class="btn">Выйти</a>
</div>
</body>
</html>