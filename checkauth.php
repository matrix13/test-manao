<?php

    $is_logged = false;
    $user = null;
    if(isset($_SESSION['login']) AND  $_SESSION['login'] != null AND $_SESSION['password']) {
        $user1 = getUser($_SESSION['login']);
        if($user1 != null AND $user1->password == $_SESSION['password']) {
            $user = $user1;
            $is_logged = true;
        } else $is_logged = false;
    } elseif(isset($_COOKIE['login']) AND $_COOKIE['login'] != null AND $_COOKIE['password']) {
        $user1 = getUser($_COOKIE['login']);
        if($user1 != null AND $user1->password == $_COOKIE['password']) {
            $user = $user1;

            $_SESSION['login'] = $_COOKIE['login'];
            $_SESSION['password'] = $_COOKIE['password'];

            $is_logged = true;
        } else $is_logged = false;
    }

    if(!$is_logged) {
        setcookie("login", "", time(), "/");
        setcookie("password", "", time(), "/");

        $_SESSION['login'] = null;
        $_SESSION['password'] = null;

        $user = null;
    }